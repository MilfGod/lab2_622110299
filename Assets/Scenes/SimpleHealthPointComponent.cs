using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealthPointComponent : MonoBehaviour
{
    [SerializeField] 
    public const float MAX_HP = 100;

    [SerializeField] 
    private float _healtPoint;

    public float HealthPoint
    {
        get
        {
            return _healtPoint;
        }
        set
        {
            if (value > 0)
            {
                _healtPoint = value;
            }
            else
            {
                _healtPoint = MAX_HP;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
